import CircularProgress from '@material-ui/core/CircularProgress';
import React, { useContext } from 'react';
import { NameContext } from '../app/app';
import withService from '../hoc/withService';
import './item-list.css';

const ItemList = ({property}) => {

  const {onElementInfo} = useContext(NameContext);

  return (
    <ul className="item-list list-group">
      {property === null ? <CircularProgress /> : property.map((item, index) => {
        return <li
          className="list-group-item"
          key={index * 3 + 'r'}
          onClick={() => onElementInfo(item)}> {item.name} </li>
      })}
    </ul>
  );

}

const ItemListWithService = withService(ItemList);

export default ItemListWithService;

