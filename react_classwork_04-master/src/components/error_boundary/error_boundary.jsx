import React, { Component } from 'react'

export default class Error_boundary extends Component {

    state = {
        hasError: false,
    }

    componentDidCatch() {
        this.setState({
            hasError: true
        })
    }

    render() {
        if (this.state.hasError) {
            <div>Error</div>
        }
        return (
            this.props.children
        )
    }
}
